const nconf = require('nconf');

// priority : arguments, followed by env variables, followed by the config file
nconf.argv().env().file({ file: '/config/config.json' });

nconf.defaults({
  rethinkdb: {
    host: 'localhost',
    port: 28015,
    db: 'status_servers',
  },
  app: {
    port: 4567,
  },
}
);

const config = {
  rhost: nconf.get('rethinkdb:host'),
  rport: nconf.get('rethinkdb:port'),
  rdb: nconf.get('rethinkdb:db'),
  appPort: nconf.get('app:port'),
  emailSmtp: nconf.get('mail:smtp'),
};

module.exports = config;
