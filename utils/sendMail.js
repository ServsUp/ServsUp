const config = require('../utils/config.js');
const nodemailer = require('nodemailer');

const smtp = config.emailSmtp;
console.log(`SMTP is, ${smtp}!`);
const transporter = smtp ? nodemailer.createTransport(smtp) : null;
if (!transporter) console.log('No SMTP configuration specified. No email will be sent.');

function sendEmail(options) {
  const mailOptions = Object.assign({}, options, {
    html: options.text,
    from: '"ServsUp 🚀" <no-reply@servsup.co>',
  });

  return new Promise((resolve, reject) => {
    // No configuration given
    if (!transporter) return reject('SMTP not configured.');

    // Checking if SMTP configuration correct
    transporter.verify((error) => {
      if (error) {
        console.log('Email support not properly configured. Email will not be sent.');
        return reject(error);
      }

      transporter.sendMail(options, (sendError, info) => {
        if (sendError) return reject(error);
        return resolve(info);
      });
    });
  });
}

module.exports = sendEmail;
