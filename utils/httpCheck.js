const fetch = require('node-fetch');

function getResponse(status, server) {
  const checks = server.checks ? [].concat.apply(server.checks) : [];
  const thisCheck = {
    time: Date.now(),
    status,
  };
  checks.push(thisCheck);
  return Object.assign({}, server, { status, checks });
}

function ping(server) {
  return new Promise((resolve, reject) => {
    fetch(server.url)
      .then(res => {
        const response = getResponse('online', server);
        resolve(response);
      })
      .catch(err => {
        const response = getResponse('unreachable', server);
        resolve(response);
      });
  });
}

module.exports = ping;
