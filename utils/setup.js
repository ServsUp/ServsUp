const r = require('rethinkdb');
const fs = require('fs');

const dbName = 'status_servers';
const dbHost = 'localhost';
const dbPort = 28015;

const tables = [
  'servers',
  'subscribers',
];

function createTable(conn) {
  return (table) => (
    new Promise((resolve, reject) => {
      r.db(dbName).tableCreate(table).run(conn, (err) => {
        if (err && err.msg === `Table \`status_servers.${table}\` already exists.`) {
          console.log(`-- Table ${table} already exists --`);
          return resolve(table);
        } else if (err) {
          return reject(err);
        }
        console.log(`-- Created table ${table} --`);
        return resolve(table);
      });
    })
  );
}

// Creating database if it doesn't already exists, with all tables
r.connect({
  host: dbHost,
  port: dbPort,
}, (err, conn) => {
  r.dbCreate(dbName).run(conn, () => {
    r.connect({
      host: dbHost,
      port: dbPort,
      db: dbName,
    }, (error, connec) => {
      const promises = tables.map(createTable(connec));
      Promise.all(promises)
        .then(() => {
          console.log('-- Done creating database --');
          process.exit(0);
        });
    });
  });
});

const configTemplate = 'tmpl/config.json';
const configFile = 'config/config.json';

// Creates config file, if it doesn't already exists
if (!fs.existsSync(configFile)) {
  console.log('Copying configuration file');
  fs.createReadStream(configTemplate).pipe(fs.createWriteStream(configFile));
}
