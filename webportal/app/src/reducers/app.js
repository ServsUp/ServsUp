import createReducer from 'create-reducer-map';
import { SOME_ACTION } from '../constants/actionHandlers';

const initialState = {
  test: null,
};

export default createReducer(initialState, {
  [SOME_ACTION]: (state, payload) => {
    return Object.assign({}, state, { test: payload.somthing });
  },
});
