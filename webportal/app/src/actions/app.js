import { SOME_ACTION } from '../constants/actionHandlers';

export function testAction(something) {
  return dispatch => dispatch({
    type: SOME_ACTION,
    payload: { something },
  });
}
