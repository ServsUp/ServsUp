import React, { Component } from 'react';
import Loader from './Loader';

const fetch = global.fetch;

function validEmail(str) {
  const filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
  return String(str).search(filter) !== -1;
}

export default class SubscribeButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
      email: '',
      isValid: true,
      pending: false,
    };
    this.toggleExpantion = this.toggleExpantion.bind(this);
    this.subscribe = this.subscribe.bind(this);
  }

  subscribe(e) {
    e.preventDefault();
    if (!validEmail(this.input.value)) {
      return this.setState({ isValid: false });
    }

    this.setState({ isValid: true, pending: true });
    return fetch('/subscribe', {
      method: 'POST',
      body: JSON.stringify({ email: this.input.value }),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((res) => {
      if (res.status === 500) throw new Error(res);
      return res.json();
    }).then(() => {
      this.setState({
        pending: false,
        expanded: false,
      });
    }).catch(() => {
      this.setState({
        pending: false,
        error: 'No SMPT configured, or not configured properly.',
      });
    });
  }

  toggleExpantion() {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  renderLoader() {
    return !this.state.error ? <Loader /> : null;
  }

  render() {
    return (
      <div className={`subscribe-container${this.state.expanded ? ' expanded' : ''}`}>
        <button type="button" onClick={this.toggleExpantion}>Subscribe</button>
        {!this.state.pending && !this.state.error ?
          <div className={`subscribe-form ${!this.state.isValid ? 'invalid' : ''}`}>
            <h3>Subscribe to changes</h3>
            <form onSubmit={this.subscribe}>
              <div className="input-container">
                <input type="email" value={this.state.value} ref={(el) => (this.input = el)} />
              </div>
              <button type="submit" onClick={this.subscribe}>Subscribe</button>
            </form>
          </div> : this.renderLoader()}
        {!this.state.pending && this.state.error && <div className="subscribe-form">
          <div>{this.state.error}</div>
          <a href="https://gitlab.com/ServsUp/ServsUp/wikis/home#configuring-email-notifications">
            Configuring Email notifications
          </a>
        </div>}
      </div>
    );
  }
}
