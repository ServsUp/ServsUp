import React, { PropTypes } from 'react';
import Close from 'react-icons/lib/md/clear';

export default function Loader(props) {
  const { children } = props;
  return (
    <div className="loader">
      <img src="/ripple.gif" alt="loading" />
    </div>
  );
}
