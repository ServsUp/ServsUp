
import rootReducer from '../reducers/index';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

const logger = createLogger({
  collapsed: true,
});

export const store = createStore(
  rootReducer,
  applyMiddleware(thunk, logger)
);
