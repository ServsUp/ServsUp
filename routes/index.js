const includes = require('lodash/includes');
const servers = require('./servers');
const status = require('./status');
const subscribers = require('./subscribers');

module.exports = [].concat(
  servers,
  status,
  subscribers,
  !includes(process.argv, '--dev') ?
    {
      method: 'GET',
      path: '/{param*}',
      handler: {
        directory: {
          path: 'public',
        },
      },
    } :
    {
      method: 'GET',
      path: '/{param*}',
      handler: {
        proxy: {
          host: 'localhost',
          port: '8080',
          protocol: 'http',
        },
      },
    }
);
