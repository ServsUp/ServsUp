const r = require('rethinkdb');
const Joi = require('joi');
const config = require('../utils/config.js');
const sendMail = require('../utils/sendMail.js');

let connection = null;

r.connect({
  host: config.rhost,
  port: config.rport,
  db: config.rdb,
}, (err, conn) => {
  if (err) throw err;
  connection = conn;
});

module.exports = [
  {
    method: 'POST',
    path: '/subscribe',
    config: {
      validate: {
        params: {
          email: Joi.string().email(),
        },
      },
    },
    handler(request, reply) {
      const subscriber = { email: request.payload.email };
      r.table('subscribers')
        .insert(subscriber)
        .run(connection, (err, result) => {
          if (err) throw err;
          const mailOptions = {
            subject: 'Welcome to Servsup.co',
            to: subscriber.email,
            text: `
You have subscribed to ServsUp.co.
From now on you will be notified of status changes of your servers.`,
          };
          sendMail(mailOptions).then(() => reply(result))
          .catch(error => reply(new Error(error)));
        });
    },
  },
  {
    method: 'GET',
    path: '/unsubscribe/{id}',
    handler(request, reply) {
      const id = request.params.id;
      r.table('subscribers')
        .get(id).delete()
        .run(connection, (err) => {
          if (err) throw err;
          // Send confirmation email here
          return reply('unsubscribed');
        });
    },
  },
];
